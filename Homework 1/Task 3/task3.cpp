/*
	��������� ���������� ���������� S ��� ������������� ��������������� �������� �� �������.
	�����������: v � ��������, t � �����, � � ���������.
*/

//��� ��� �����

#include <iostream>
#include <windows.h>
#include <iomanip>

#define OUT std::cout
#define OUTN std::cout << '\n'
#define IN std::cin

int main()
{
	SetConsoleOutputCP(1251);

	OUT << "������� �������� �������� (�/�): ";
	float v;
	IN >> v;

	OUTN << "������� �������� ���������� (�/�2): ";
	float a;
	IN >> a;

	OUTN << "������� �������� ���� (c): ";
	float t;
	IN >> t;

	float s = v * t + a * t * t;
	
	OUTN << "\n����������: " << s << " (�)";

	OUTN << "� ����� ����! :)\n\n";
}