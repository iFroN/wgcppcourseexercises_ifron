/*
	������������ ������ � ���������� ����� � ��������, ��������� � ������ ���.
	������� �� ����� ������� ����� � �����, ������� � ��������.
	���������, ������� �����, ����� � ������ �������� �� ��������.
*/

//��� ��� �����

#include <iostream>
#include <windows.h>
#include <iomanip>

#define OUT std::cout
#define OUTN std::cout << '\n'
#define IN std::cin

#define WEEK_SIZE 7

int main()
{

	SetConsoleOutputCP(1251);

	time_t now = time(0);

	OUT << std::setw(15) << std::left << "������� ���: " << ctime(&now);
	OUT << std::setw(15) << std::left << "��� UTC: " << asctime(gmtime(&now));


	tm * str_now = localtime(&now);
	str_now->tm_hour = 0;
	str_now->tm_min = 0;
	str_now->tm_sec = 0;

	time_t midnight = mktime(str_now);

	OUTN << "������� ��� � �������� �� ������� ������: ";

	unsigned inputTime;
	IN >> inputTime;

	str_now->tm_sec += inputTime;

	time_t custom = mktime(str_now);

	OUT << std::setw(15) << std::left << "���� ���������: " << asctime(str_now);


	str_now->tm_sec += inputTime;

	tm * tm_midnight = localtime(&midnight);

	tm_midnight->tm_mday += 1;
	tm_midnight->tm_sec = difftime(midnight, custom);;

	mktime(tm_midnight);

	char buffer[10];

	strftime(buffer, sizeof(buffer), "%H:%M:%S", tm_midnight);

	OUT << "�� ����� ������ ����������: "<< buffer << "\n\n";

}