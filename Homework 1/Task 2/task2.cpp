/*
	������ ��� ������������� R1, R2, R3 ����������� ������������ �����������. ��������� �������� ������ ������������� R ����.
	����������� ������: R1=2, R2=4, R3=8 R = 1.142857
*/

//��� ��� �����

#include <iostream>
#include <windows.h>
#include <iomanip>

#define OUT std::cout
#define OUTN std::cout << '\n'
#define IN std::cin

int main()
{
	SetConsoleOutputCP(1251);

	OUT << "������� ��������� �������� ���������� ��������: ";

	unsigned count;
	IN >> count;

	float r;
	float sr = 0;

	for (unsigned i = 1; i <= count; ++i)
	{
		OUTN << '\n' << i << ": ";
		IN >> r;
		sr += 1 / r;
		OUT << "������ �����: " << std::setprecision(7) << 1/sr;
	}

	OUTN << "\n�������� ������������� �������: " << std::setprecision(7) << 1 / sr;

	OUTN << "������, ��� ����������� ����� ������� ���������! ����������� ����! :)\n";
}