/*
	�������� ���������, ������� ����������� ��������� ������������� ����������
	���� � ���������� ������ ������ � ���������� ����. ��������, ������������ ���� 17 ����,
	��������� ������ ������� �� ����� 2 ������ � 3 ���.
*/

//��� ��� �����

#include <iostream>
#include <windows.h>
#include <iomanip>

#define OUT std::cout
#define IN std::cin

#define WEEK_SIZE 7

int main()
{

	SetConsoleOutputCP(1251);

	OUT << "������� ��������� ���: ";

	unsigned days;
	IN >> days;

	OUT << "����� ������: " << days / WEEK_SIZE << ". � ���� ���: " << days % WEEK_SIZE << ".\n";

	OUT << "����������, �� �� ��������! :)\n\n";

}