/*
	������������ ������ � ���������� ����������, ������ ������� �� 100 �� � ��������� ���� ����� �������.
	������� �� ����� ������������� ������� �� ���������� ������� �� ������ ����� �������.
*/

//��� ��� �����

#include <iostream>
#include <windows.h>
#include <iomanip>

#define OUT std::cout
#define OUTN std::cout << '\n'
#define FILL(src, count) std::setfill(src) << std::setw(count)
#define LINE(src) OUTN << FILL(src, 47) << '\n';
#define IN std::cin
#define VB "|"

#define WEEK_SIZE 7

int main()
{

	SetConsoleOutputCP(1251);

	OUT << "������� ��������� ���� �����, ��� ������ ��������: ";

	int count;
	IN >> count;

	OUTN << "������� �������� ������� ���� �����:\n";

	float min = FLT_MAX;
	unsigned minIndex = 0;

	float * fluelCost = new float[count];

	for (unsigned i = 0; i < count; ++i)
	{
		OUT << i + 1 << ": ";
		IN >> fluelCost[i];
		if (fluelCost[i] < min)
		{
			min = fluelCost[i];
			minIndex = i;
		}
	}

	OUT << "������� ����������, �� ���� ������ ���������� (��): ";

	float distance;
	IN >> distance;


	OUT << "������� ������ ����� �� 100 �� (�): ";

	float consumption;
	IN >> consumption;

	OUT << "������ �� ����������!\n";

	OUTN << "���� ����������� ������:";
	
	LINE('=')

	OUT << VB << " ��� ����� " << VB << " ���� 1 ����� " << VB << " ���� ��������� " << VB << std::setprecision(3);

	LINE('=')

	for (unsigned i = 0; i < count; ++i)
	{
		OUT << VB
		<< FILL(' ', 9) << i + 1 << (minIndex == i ? ".* " : ".  ") << VB
		<< std::setw(13) << fluelCost[i] << ' ' << VB
		<< std::setw(15) << fluelCost[i] * distance / 100.f * consumption << ' ' << VB;
		LINE('-')
	}

	delete[] fluelCost;

	OUT << "* - ����������� �������.\n";
	OUTN << "��������� ���������! :)\n\n";

}